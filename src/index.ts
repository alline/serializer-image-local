import {
  downloadFile,
  EpisodeContext,
  ModelSerializer,
  resolveEpisodeName,
  resolveSeasonName
} from "@alline/core";
import { createWriteStream } from "fs";
import { mkdir, rename } from "fs/promises";
import path from "path";
import fileType from "file-type";

export interface LocalImageSerializerOption {
  baseDir: string;
  mkdir?: boolean;
}

export class LocalImageSerializer extends ModelSerializer<string[]> {
  protected baseDir: string;
  protected mkdir: boolean;

  constructor(option: LocalImageSerializerOption) {
    super();
    const { baseDir, mkdir = true } = option;
    this.baseDir = baseDir;
    this.mkdir = mkdir;
  }

  protected async onSerialize(
    images: string[],
    ctx: EpisodeContext
  ): Promise<void> {
    const { season, episode, title } = ctx;
    const dir = path.resolve(this.baseDir, title, resolveSeasonName(season));
    if (this.mkdir) {
      await mkdir(dir);
    }
    const promises = images.map(async (image, i) => {
      const suffix = i > 1 ? `${i - 1}` : undefined;
      const name = resolveEpisodeName(title, season, episode, suffix);
      const tmp = path.format({ dir, name, ext: ".tmp" });
      await downloadFile(image, createWriteStream(tmp));
      const type = await fileType.fromFile(tmp);
      if (type) {
        const { ext } = type;
        const fileName = path.format({ dir, name, ext });
        await rename(tmp, fileName);
      } else {
        throw new Error(`Unknown type: ${image}`);
      }
    });
    await Promise.all(promises);
  }
}
